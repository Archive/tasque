# tasque ja.po.
# Copyright (C) 2008 Free Software Foundation, Inc.
# This file is distributed under the same license as the tasque package.
# Takeshi AIHANA <takeshi.aihana@gmail.com>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: tasque trunk\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-06-03 19:29+0200\n"
"PO-Revision-Date: 2012-01-08 13:05+0900\n"
"Last-Translator: Jiro Matsuzawa <jmatsuzawa@src.gnome.org>\n"
"Language-Team: Japanese <takeshi.aihana@gmail.com>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../data/tasque.desktop.in.h:1
msgid "Tasque"
msgstr "Tasque"

#: ../data/tasque.desktop.in.h:2
msgid "Easy quick task management"
msgstr "素早く簡単にタスクを管理します"

#: ../data/tasque.desktop.in.h:3
msgid "Task Manager"
msgstr "タスク・マネージャー"

#: ../src/Gtk.Tasque/CompletedTaskGroup.cs:167
#: ../src/Gtk.Tasque/Utilities.cs:283
msgid "Yesterday"
msgstr "昨日"

#: ../src/Gtk.Tasque/CompletedTaskGroup.cs:169
msgid "Last 7 Days"
msgstr "この１週間"

#: ../src/Gtk.Tasque/CompletedTaskGroup.cs:171
msgid "Last Month"
msgstr "この１ヶ月"

#: ../src/Gtk.Tasque/CompletedTaskGroup.cs:173
msgid "Last Year"
msgstr "この１年"

#: ../src/Gtk.Tasque/CompletedTaskGroup.cs:176
#: ../src/libtasque/Utils/AllList.cs:85
msgid "All"
msgstr "全て"

#: ../src/Gtk.Tasque/Defines.cs.in:59
#, fuzzy
msgid "Copyright © 2008 - 2011 Novell, Inc and contributors."
msgstr "Copyright © 2008 Novell, Inc."

#: ../src/Gtk.Tasque/GtkTray.cs:87
#, csharp-format
msgid "{0} task is Overdue"
msgid_plural "{0} tasks are Overdue"
msgstr[0] ""
msgstr[1] ""

#: ../src/Gtk.Tasque/GtkTray.cs:97
#, csharp-format
msgid "{0} task for Today"
msgid_plural "{0} tasks for Today"
msgstr[0] ""
msgstr[1] ""

#: ../src/Gtk.Tasque/GtkTray.cs:108
#, csharp-format
msgid "{0} task for Tomorrow"
msgid_plural "{0} tasks for Tomorrow"
msgstr[0] ""
msgstr[1] ""

#. Translators: This is the status icon's tooltip. When no tasks are overdue, due today, or due tomorrow, it displays this fun message
#: ../src/Gtk.Tasque/GtkTray.cs:115
#, fuzzy
msgid "Tasque Rocks"
msgstr "Tasque"

#: ../src/Gtk.Tasque/GtkTray.cs:138
msgid "translator-credits"
msgstr "相花 毅 <takeshi.aihana@gmail.com>"

#: ../src/Gtk.Tasque/GtkTray.cs:147
#, fuzzy
msgid "A Useful Task List"
msgstr "意外と便利なタスクの一覧です。"

#: ../src/Gtk.Tasque/GtkTray.cs:149
msgid "Tasque Project Homepage"
msgstr "Tasque のプロジェクト・ページ"

#: ../src/Gtk.Tasque/GtkTray.cs:161
#, fuzzy
msgid "New Task ..."
msgstr "新しいタスク..."

#: ../src/Gtk.Tasque/GtkTray.cs:168
msgid "Refresh Tasks ..."
msgstr "タスクの更新..."

#: ../src/Gtk.Tasque/GtkTray.cs:184
msgid "Toggle Task Window"
msgstr ""

#: ../src/Gtk.Tasque/NoteDialog.cs:26
#, csharp-format
msgid "Notes for: {0:s}"
msgstr "{0:s} のメモ"

#. Update the window title
#: ../src/Gtk.Tasque/PreferencesDialog.cs:103
msgid "Tasque Preferences"
msgstr ""

#: ../src/Gtk.Tasque/PreferencesDialog.cs:122
msgid "General"
msgstr "全般"

#: ../src/Gtk.Tasque/PreferencesDialog.cs:130
msgid "Appearance"
msgstr ""

#: ../src/Gtk.Tasque/PreferencesDialog.cs:166
msgid "Color Management"
msgstr ""

#: ../src/Gtk.Tasque/PreferencesDialog.cs:177
#, fuzzy
msgid "Today:"
msgstr "今日"

#: ../src/Gtk.Tasque/PreferencesDialog.cs:205
#, fuzzy
msgid "Overdue:"
msgstr "期限切れ"

#: ../src/Gtk.Tasque/PreferencesDialog.cs:249
#, fuzzy
msgid "Task Management System"
msgstr "タスク管理システム"

#: ../src/Gtk.Tasque/PreferencesDialog.cs:291
#, fuzzy
msgid "Task Filtering"
msgstr "タスクのフィルタリング"

#: ../src/Gtk.Tasque/PreferencesDialog.cs:310
msgid "Sh_ow completed tasks"
msgstr "全てのタスクを表示する(_O)"

#. TaskLists TreeView
#: ../src/Gtk.Tasque/PreferencesDialog.cs:319
#, fuzzy
msgid "Only _show these lists when \"All\" is selected:"
msgstr "\"全て\" を選択したら次のカテゴリのみ表示する(_S):"

#: ../src/Gtk.Tasque/PreferencesDialog.cs:337
#, fuzzy
msgid "Task List"
msgstr "意外と便利なタスクの一覧です。"

#: ../src/Gtk.Tasque/RemoteControl.cs:177
msgid "New task created."
msgstr "新しいタスクを生成しました"

#. The new task entry widget
#. Clear the entry if it contains the default text
#: ../src/Gtk.Tasque/TaskWindow.cs:156 ../src/Gtk.Tasque/TaskWindow.cs:818
#: ../src/Gtk.Tasque/TaskWindow.cs:874 ../src/Gtk.Tasque/TaskWindow.cs:884
#: ../src/Gtk.Tasque/TaskWindow.cs:895
msgid "New task..."
msgstr "新しいタスク..."

#: ../src/Gtk.Tasque/TaskWindow.cs:172
msgid "_Add"
msgstr "追加(_A)"

#: ../src/Gtk.Tasque/TaskWindow.cs:176
#, fuzzy
msgid "_Add Task"
msgstr "タスクの追加(_A)"

#: ../src/Gtk.Tasque/TaskWindow.cs:260
msgid "Overdue"
msgstr "期限切れ"

#: ../src/Gtk.Tasque/TaskWindow.cs:277 ../src/Gtk.Tasque/Utilities.cs:277
#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:51
#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:118
msgid "Today"
msgstr "今日"

#: ../src/Gtk.Tasque/TaskWindow.cs:294 ../src/Gtk.Tasque/Utilities.cs:296
#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:53
#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:121
msgid "Tomorrow"
msgstr "明日"

#: ../src/Gtk.Tasque/TaskWindow.cs:311
msgid "Next 7 Days"
msgstr "次の１週間"

#: ../src/Gtk.Tasque/TaskWindow.cs:327
msgid "Future"
msgstr "将来"

#: ../src/Gtk.Tasque/TaskWindow.cs:340
#: ../src/Addins/Gtk.Tasque.Columns/CompleteColumn.cs:40
#: ../src/Addins/Gtk.Tasque.TimerCompleteColumns/CompleteWithTimerColumn.cs:40
msgid "Completed"
msgstr "完了"

#. Translators: This status shows the date and time when the task list was last refreshed
#: ../src/Gtk.Tasque/TaskWindow.cs:477 ../src/Gtk.Tasque/TaskWindow.cs:1210
#, csharp-format
msgid "Tasks loaded: {0}"
msgstr ""

#. Show error status
#: ../src/Gtk.Tasque/TaskWindow.cs:782
msgid "Error creating a new task"
msgstr "新しいタスクを生成する際にエラーが発生しました"

#. Show successful status
#: ../src/Gtk.Tasque/TaskWindow.cs:786
#, fuzzy
msgid "Task created successfully"
msgstr "タスクの生成が完了しました"

#: ../src/Gtk.Tasque/TaskWindow.cs:1070
msgid "_Notes..."
msgstr "メモ(_N)..."

#: ../src/Gtk.Tasque/TaskWindow.cs:1077
msgid "_Delete task"
msgstr "タスクの削除(_D)"

#: ../src/Gtk.Tasque/TaskWindow.cs:1082
msgid "_Edit task"
msgstr "タスクの編集(_E)"

#. TODO Needs translation.
#: ../src/Gtk.Tasque/TaskWindow.cs:1114
#, fuzzy
msgid "_Change list"
msgstr "カテゴリ"

#: ../src/Gtk.Tasque/TaskWindow.cs:1172
#, fuzzy
msgid "Task deleted"
msgstr "タスクを削除しました"

#: ../src/Gtk.Tasque/Utilities.cs:275
#, csharp-format
msgid "Today, {0}"
msgstr "今日の{0}"

#: ../src/Gtk.Tasque/Utilities.cs:281
#, csharp-format
msgid "Yesterday, {0}"
msgstr "昨日の{0}"

#: ../src/Gtk.Tasque/Utilities.cs:287
#, csharp-format
msgid "{0} days ago, {1}"
msgstr "{0}日前の{1}"

#: ../src/Gtk.Tasque/Utilities.cs:289
#, csharp-format
msgid "{0} days ago"
msgstr "{0}日前"

#: ../src/Gtk.Tasque/Utilities.cs:294
#, csharp-format
msgid "Tomorrow, {0}"
msgstr "明日の{0}"

#: ../src/Gtk.Tasque/Utilities.cs:300
#, csharp-format
msgid "In {0} days, {1}"
msgstr "{0}日以内の{1}"

#: ../src/Gtk.Tasque/Utilities.cs:302
#, csharp-format
msgid "In {0} days"
msgstr "{0}日以内"

#: ../src/Gtk.Tasque/Utilities.cs:306
msgid "MMMM d, h:mm tt"
msgstr "MMMM d, h:mm tt"

#: ../src/Gtk.Tasque/Utilities.cs:307
msgid "MMMM d"
msgstr "MMMM d"

#: ../src/Gtk.Tasque/Utilities.cs:309
#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:66
#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:123
msgid "No Date"
msgstr "期日なし"

#: ../src/Gtk.Tasque/Utilities.cs:312
msgid "MMMM d yyyy, h:mm tt"
msgstr "MMMM d yyyy, h:mm tt"

#: ../src/Gtk.Tasque/Utilities.cs:313
msgid "MMMM d yyyy"
msgstr "MMMM d yyyy"

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:78
msgid "Click Here to Connect"
msgstr "ここをクリックして接続して下さい"

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:84
#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:153
#, fuzzy
msgid "You are currently connected"
msgstr ""
"\n"
"\n"
"現在接続中です"

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:88
#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:157
#, fuzzy
msgid "You are currently connected as"
msgstr ""
"\n"
"\n"
"現在接続中です"

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:92
#, fuzzy
msgid "You are not connected"
msgstr ""
"\n"
"\n"
"接続していません"

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:123
msgid "Remember the Milk not responding. Try again later."
msgstr ""
"Remember the Milk から応答がありません (あとでもう一度試してみて下さい)"

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:130
msgid "Click Here After Authorizing"
msgstr "認証が完了したらここをクリックして下さい"

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:133
msgid "Set the default browser and try again"
msgstr "デフォルトのブラウザーを指定してから試して下さい"

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:136
msgid "Processing..."
msgstr ""

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:146
msgid "Failed, Try Again"
msgstr ""

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:150
msgid "Thank You"
msgstr "サンキュー"

#: ../src/Addins/Gtk.Tasque.Columns/CompleteColumn.cs:81
#: ../src/Addins/Gtk.Tasque.TimerCompleteColumns/CompleteWithTimerColumn.cs:98
msgid "Task Completed"
msgstr "完了したタスク"

#: ../src/Addins/Gtk.Tasque.Columns/CompleteColumn.cs:83
#: ../src/Addins/Gtk.Tasque.TimerCompleteColumns/CompleteWithTimerColumn.cs:114
msgid "Action Canceled"
msgstr "アクションをキャンセルしました"

#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:40
msgid "Due Date"
msgstr "期日"

#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:51
#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:53
#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:65
#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:118
#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:120
#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:125
msgid "M/d - "
msgstr "M/d - "

#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:55
#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:57
#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:59
#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:61
#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:63
#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:135
#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:189
msgid "M/d - ddd"
msgstr "M/d - ddd曜日"

#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:65
#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:126
msgid "In 1 Week"
msgstr "１週間以内"

#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:67
#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:128
msgid "Choose Date..."
msgstr "日付の選択..."

#: ../src/Addins/Gtk.Tasque.Columns/DueDateColumn.cs:191
msgid "M/d/yy - ddd"
msgstr "yy/M/d - ddd曜日"

#: ../src/Addins/Gtk.Tasque.Columns/NotesColumn.cs:47
msgid "Notes"
msgstr "メモ"

#: ../src/Addins/Gtk.Tasque.Columns/PriorityColumn.cs:40
msgid "Priority"
msgstr "優先度"

#: ../src/Addins/Gtk.Tasque.Columns/PriorityColumn.cs:49
#: ../src/Addins/Gtk.Tasque.Columns/PriorityColumn.cs:89
#: ../src/Addins/Gtk.Tasque.Columns/PriorityColumn.cs:150
msgid "1"
msgstr "1"

#. High
#: ../src/Addins/Gtk.Tasque.Columns/PriorityColumn.cs:50
#: ../src/Addins/Gtk.Tasque.Columns/PriorityColumn.cs:87
#: ../src/Addins/Gtk.Tasque.Columns/PriorityColumn.cs:147
msgid "2"
msgstr "2"

#. Medium
#: ../src/Addins/Gtk.Tasque.Columns/PriorityColumn.cs:51
#: ../src/Addins/Gtk.Tasque.Columns/PriorityColumn.cs:85
#: ../src/Addins/Gtk.Tasque.Columns/PriorityColumn.cs:144
msgid "3"
msgstr "3"

#. Low
#: ../src/Addins/Gtk.Tasque.Columns/PriorityColumn.cs:52
#: ../src/Addins/Gtk.Tasque.Columns/PriorityColumn.cs:153
msgid "-"
msgstr "-"

#: ../src/Addins/Gtk.Tasque.Columns/TaskNameColumn.cs:42
msgid "Task Name"
msgstr "タスクの名前"

#: ../src/Addins/Gtk.Tasque.TimerCompleteColumns/CompleteWithTimerColumn.cs:108
#, csharp-format
msgid "Completing Task In: {0}"
msgstr "{0} で完了したタスク"

#: ../src/Addins/Gtk.Tasque.TimerCompleteColumns/TimerColumn.cs:43
msgid "Timer"
msgstr "タイマー"

#. Token "O". Notice only the suffix is translated.
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/DateFormatters/TaskParser.cs:49
msgid "th,st,nd,rd"
msgstr ""

#. Token "T"
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/DateFormatters/TaskParser.cs:53
#, fuzzy
msgid "today"
msgstr "今日"

#. Token "T"
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/DateFormatters/TaskParser.cs:57
#, fuzzy
msgid "tomorrow"
msgstr "明日"

#. Token "m". Don't forget to include the plural value, if any
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/DateFormatters/TaskParser.cs:61
msgid "month|months"
msgstr ""

#. Token "w". Don't forget to include the plural value, if any
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/DateFormatters/TaskParser.cs:65
msgid "week|weeks"
msgstr ""

#. Token "y". Don't forget to include the plural value, if any
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/DateFormatters/TaskParser.cs:69
msgid "year|years"
msgstr ""

#. Token "d". Don't forget to include the plural value, if any
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/DateFormatters/TaskParser.cs:73
msgid "day|days"
msgstr ""

#. Token "u". More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/DateFormatters/TaskParser.cs:207
msgid "due before|due by|due"
msgstr ""

#. Token "n". Examples could be: "Next Month", "Next Monday"
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/DateFormatters/TaskParser.cs:233
msgid "next"
msgstr ""

#. Token "o". Examples could be: "On April 1st", "On Wednesday"
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/DateFormatters/TaskParser.cs:239
msgid "on"
msgstr ""

#. Token "i". Examples could be: "In 2 weeks", "In 3 months"
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/DateFormatters/TaskParser.cs:245
msgid "in"
msgstr ""

#. Represents "Today and Tomorrow" expression.
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/DateFormatters/TaskParser.cs:260
msgid "u T"
msgstr ""

#. Represents: "Next" expression.
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/DateFormatters/TaskParser.cs:263
msgid "n w|n m|n y|n D"
msgstr ""

#. Represents "Due" expression.
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/DateFormatters/TaskParser.cs:266
msgid "u o D|u M O|u M N|u O|u M"
msgstr ""

#. Represents "On" expression.
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/DateFormatters/TaskParser.cs:269
msgid "o D|o O"
msgstr ""

#. Represents "In" expression.
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/DateFormatters/TaskParser.cs:272
msgid "i N d|i d|i N w|i w|i N m|i m|i N y|i y"
msgstr ""

#. Represents all other expressions not using tokens.
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/DateFormatters/TaskParser.cs:275
msgid "T|D|M O|M N|O|A"
msgstr ""

#~ msgid "Category"
#~ msgstr "カテゴリ"

#~ msgid "Loading tasks..."
#~ msgstr "タスクの読み込み中..."

#, fuzzy
#~ msgid "Not connected."
#~ msgstr ""
#~ "\n"
#~ "\n"
#~ "接続していません"

#~ msgid "_File"
#~ msgstr "ファイル(_F)"

#~ msgid "_Window"
#~ msgstr "ウィンドウ(_W)"

#~ msgid "Show Tasks ..."
#~ msgstr "タスクの表示..."

#, fuzzy
#~ msgid "New task1..."
#~ msgstr "新しいタスク..."

#~ msgid "Sunday"
#~ msgstr "日曜日"

#~ msgid "Monday"
#~ msgstr "月曜日"

#~ msgid "Tuesday"
#~ msgstr "火曜日"

#~ msgid "Wednesday"
#~ msgstr "水曜日"

#~ msgid "Thursday"
#~ msgstr "木曜日"

#~ msgid "Friday"
#~ msgstr "金曜日"

#~ msgid "Saturday"
#~ msgstr "土曜日"

#~ msgid "^(?<task>.+)\\s+today\\W*$"
#~ msgstr "^(?<task>.+)\\s+今日\\W*$"

#~ msgid "^(?<task>.+)\\s+tomorrow\\W*$"
#~ msgstr "^(?<task>.+)\\s+明日\\W*$"
